﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class PlayerTopDown : Player
{
    KeyValuePair<Cell, float>[] neighbourLinkCache = new KeyValuePair<Cell, float>[4];
   
    public override IEnumerable<KeyValuePair<Cell, float>> GraphExplorer(Cell cell)
    {
        // A directional heuristic could provide an optimisation here.
        neighbourLinkCache[0] = new KeyValuePair<Cell, float>(cell.Up,    1f);
        neighbourLinkCache[1] = new KeyValuePair<Cell, float>(cell.Right, 1f);
        neighbourLinkCache[2] = new KeyValuePair<Cell, float>(cell.Down,  1f);
        neighbourLinkCache[3] = new KeyValuePair<Cell, float>(cell.Left,  1f);
        return neighbourLinkCache;
    }
}
