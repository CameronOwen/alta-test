﻿public sealed class ClimbMove : PlayerMove
{
    public override (float cost, Cell target) Evaluate(Cell from, GridData grid, Direction direction)
    {
        var x = direction == Direction.Left ? -1 : 1;
        
        // Early out for blocks above players head
        if (grid.IsObstructed(from.Up))
        {
            return (0f, from);
        }
        
        // [X][X]  [ ][G]
        // [ ][G]  [ ][#]
        // [F][#]  [F][X]

        // Test low ledge
        var target = from.Offset(x, 1);
        if (!grid.IsObstructed(target) && grid.IsObstructed(target.Down)) 
        {
            return (cost * 3, target);
        }

        // Test High ledge
        target = from.Offset(x, 2);
        if (!grid.IsObstructed(target) &&
            !grid.IsObstructed(from.Up.Up) &&
             grid.IsObstructed(target.Down))
        {
            return (cost * 4, target);
        }

        // High and low ledge tests failed.
        return (0f, from);
    }





}
