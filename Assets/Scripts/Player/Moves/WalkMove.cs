﻿public sealed class WalkMove : PlayerMove
{
    public override (float cost, Cell target) Evaluate(Cell from, GridData grid, Direction direction)
    {
        var target   = from.Offset(direction == Direction.Left ? -1 : +1, 0);
        var hasSpace = !grid.IsObstructed(target);

        if (!hasSpace)
        {
            return (0f, from); // move blocked
        }

        return EvaluateFall(target, grid, cost);

    }

}
