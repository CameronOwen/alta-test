﻿public sealed class JumpMove : PlayerMove
{
    public override (float cost, Cell target) Evaluate(Cell from, GridData grid, Direction direction)
    {
        var x = direction == Direction.Left ? -1 : 1;
        var target = from.Offset(2 * x, 0);
        if (grid.IsObstructed(from.Up) || 
            grid.IsObstructed(from.Offset(1 * x,1)) ||
            grid.IsObstructed(from.Offset(2 * x,1)) ||
            grid.IsObstructed(from.Offset(1 * x,0)) ||
            grid.IsObstructed(target))
        {
            return (0f, from); // Not clear for jumping...
        }

        return EvaluateFall(target, grid, cost * 6f);
    }
}
