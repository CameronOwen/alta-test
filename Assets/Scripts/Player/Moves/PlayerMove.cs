using UnityEngine;

[ExecuteAlways]
[RequireComponent(typeof(PlayerPlatformer))]
public abstract class PlayerMove : MonoBehaviour
{
    public enum Direction
    {
        Left,
        Right
    }

    [SerializeField]
    protected float cost = 1f;

    protected PlayerPlatformer player;

    /// <summary>
    /// Searches the map for valid moves from the given cell and returns goal cell and cost.
    /// If there is no valid move it returns the given cell with a cost of zero. 
    /// </summary>
    public abstract (float cost, Cell target) Evaluate(Cell from, GridData map, Direction direction);

    protected virtual void Awake()
    {
        player = GetComponent<PlayerPlatformer>();        
    }

    protected virtual void OnEnable()
    {
        player.AddMove(this);
    }
    protected virtual void OnDisable()
    {
        player.RemoveMove(this);
    }

    /// <summary>
    /// Evaluates a fall from the given grid cell up to a maximum of PlayerPlatformer.MaxFallDistance.
    /// </summary>
    protected (float cost, Cell target) EvaluateFall(Cell from, GridData grid, float startingCost)
    {
        player = player ?? GetComponent<PlayerPlatformer>();
        var fall = 0;
        while (fall < player.MaxFallDistance)
        {
            // If floor found
            if (grid.IsObstructed(from.Offset(0, -(fall + 1))))
            {
                return (startingCost + cost * fall, from.Offset(0, -fall));
            }
            fall++;
        }
        return (0f, from); // could not find floor after max distance searched.
    }
}
