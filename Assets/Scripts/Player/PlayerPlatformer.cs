﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public sealed class PlayerPlatformer : Player
{
    // Used by some moves to have safe constraints in unbounded grid system.
    [SerializeField] int maxFallDistance = 256;

    NavGraph navGraph = new NavGraph();

    public int MaxFallDistance => maxFallDistance;

    private readonly HashSet<PlayerMove> moves = new HashSet<PlayerMove>();
    public HashSet<PlayerMove> Moves => moves;

    protected override void Awake()
    {
        base.Awake();
        UpdateGraph();
    }

    protected override void OnMapTileChanged(Tile Tile)
    {
        base.OnMapTileChanged(Tile);
        UpdateGraph();
    }

    protected override void NavigateTo(Cell target)
    {
        // Find floor tile under click.
        for(var limit = 0; limit < maxFallDistance; limit++)           
        {
            if (map.RuntimeData.IsObstructed(target.Down))
            {
                break;
            }
            target = target.Down;
        }
        targetCell = target;
    }

    internal void UpdateGraph()
    {
        if (map == null) { return; }
        navGraph.Update(map.RuntimeData, this);
    }

    public void AddMove(PlayerMove move)
    {
        if (moves.Add(move)) { UpdateGraph(); }
    }
    public void RemoveMove(PlayerMove move)
    {
        if (moves.Remove(move)) { UpdateGraph(); }
    }

    protected override IEnumerator NavigateToTarget()
    {
        UpdateGraph();
        return base.NavigateToTarget();
    }

    public override IEnumerable<KeyValuePair<Cell, float>> GraphExplorer(Cell cell)
    {
        return navGraph.Nodes[cell].Links;
    }

    internal (Map map, NavGraph navGraph, HashSet<PlayerMove> moves) GetDebugInfo()
    {
        return (map, navGraph, moves);
    }

}
