using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public abstract class Player : MonoBehaviour
{
    [SerializeField] protected Cell startCell;
    [SerializeField] protected float speed = 3f;
    [SerializeField] protected PathFinder pathFinder;
    [SerializeField] protected Map map;

    protected Cell currentCell;
    protected Cell targetCell;
    protected bool mapUpdated = false;

    Camera mainCamera;
    protected virtual void Awake()
    {
        mainCamera = Camera.main;
        currentCell = startCell;
        map.TileChanged += OnMapTileChanged;
    }

    protected virtual void Start()
    {
        targetCell = currentCell;
        StartCoroutine(NavigateToTarget());
    }

    protected virtual void OnValidate()
    {
        if (pathFinder == null) { return; }
        transform.position = map.CellToWorld(startCell);
    }

    protected virtual void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            var mouseScreenPos = Input.mousePosition;
            var mouseWorldRay = mainCamera.ScreenPointToRay(mouseScreenPos);
            var mouseWorldPos = mouseWorldRay.origin;

            // Assumes 2D scene view & grid is aligned
            // with scene view z plane. 
            mouseWorldPos.z = map.transform.position.z;

            NavigateTo(map.WorldToCell(mouseWorldPos));
        }
    }

    protected virtual void OnMapTileChanged(Tile obj) 
    {
        mapUpdated = true;
    }

    protected virtual void NavigateTo(Cell target)
    {
        targetCell = target;
    }

    public abstract IEnumerable<KeyValuePair<Cell, float>> GraphExplorer(Cell cell);

    protected virtual IEnumerator NavigateToTarget()
    {
        var currentTarget = targetCell;
        var path = pathFinder.FindPath(map.RuntimeData, currentCell, currentTarget, GraphExplorer);

        mapUpdated = false;

        // Wait for target to change.
        if (path.Count < 2)
        {
            while (!mapUpdated && currentTarget == targetCell)
            {
                yield return null;
            }
            StartCoroutine(NavigateToTarget());
            yield break;
        }


        transform.position = map.CellToWorld(path[0]);
        yield return null;

        var elapsed = 0f;

        for (var i = 0; i < path.Count - 1; i++)
        {

            var c0 = path[i];
            var c1 = path[i + 1];
            var p0 = map.CellToWorld(c0);
            var p1 = map.CellToWorld(c1);

            currentCell = c1;

            var duration = Vector3.Distance(p0, p1) / speed;

            while (elapsed < duration)
            {
                elapsed += Time.smoothDeltaTime;
                if (elapsed <= duration)
                {
                    transform.position = Vector3.Lerp(p0, p1, elapsed / duration);
                    yield return null;
                }
            }

            // Target changed, repath...
            if (mapUpdated || currentTarget != targetCell)
            {
                StartCoroutine(NavigateToTarget());
                yield break;
            }

            elapsed -= duration; // retains movement into next path segment
        }

        transform.position = map.CellToWorld(path[path.Count - 1]);

        // Watch for target change.
        while (true)
        {
            if (currentTarget != targetCell)
            {
                StartCoroutine(NavigateToTarget());
                yield break;
            }
            yield return null;
        }
    }

}
