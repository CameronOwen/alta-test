﻿using System.Runtime.CompilerServices;
using UnityEngine;

public static class GridExtensions
{
    /// <summary>
    /// Returns the local bounds for a cell at the location taking the cell gap into account.
    /// </summary>
    /// <remarks>This is a workaround for a bug in the grid component that doesn't
    /// accurately take the cell gap into account.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Bounds GetBoundsLocalFixed(this Grid grid, Vector3Int cell)
    {
        var bounds = grid.GetBoundsLocal(cell);
        bounds.center += bounds.extents;
        return bounds;
    }

    /// <summary>
    /// Converts a world position to cell position.
    /// </summary>
    /// <remarks>This is a workaround for a bug in the grid component that doesn't
    /// accurately take the cell gap into account.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3Int WorldToCellFixed(this Grid grid, Vector3 worldPosition)
    {
        var offset = grid.transform.TransformVector(grid.cellGap) * 0.5f;
        return grid.WorldToCell(worldPosition + offset);
    }

    /// <summary>
    /// Converts a local position to cell position.
    /// </summary>
    /// <remarks>This is a workaround for a bug in the grid component that doesn't
    /// accurately take the cell gap into account.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3Int LocalToCellFixed(this Grid grid, Vector3 localPosition)
    {
        var offset = grid.transform.TransformVector(grid.cellGap) * 0.5f;
        return grid.LocalToCell(localPosition + offset);
    }

    /// <summary>
    /// Get the logical center coordinate of a grid cell in local space.
    /// </summary>
    /// <remarks>This is a workaround for a bug in the grid component that doesn't
    /// accurately take the cell gap into account.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 GetCellCenterLocalFixed(this Grid grid, Vector3Int cell)
    {
        var offset = grid.transform.TransformVector(grid.cellGap) * 0.5f;
        return grid.GetCellCenterLocal(cell) - offset;
    }

    /// <summary>
    /// Get the logical center coordinate of a grid cell in world space.
    /// </summary>
    /// <remarks>This is a workaround for a bug in the grid component that doesn't
    /// accurately take the cell gap into account.</remarks>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector3 GetCellCenterWorldFixed(this Grid grid, Vector3Int cell)
    {
        var offset = grid.transform.TransformVector(grid.cellGap) * 0.5f;
        return grid.GetCellCenterWorld(cell) - offset;
    }


}
