﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

public static class CollectionExtensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T ItemClamped<T>(this IList<T> list, int index)
    {
        var count = list.Count;
        return index < 0 ? list[0] : index >= count ? list[count - 1] : list[index];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T ItemWrapped<T>(this IList<T> list, int index)
    {
        var count = list.Count;
        return index < 0 ? list[count - 1] : index >= count ? list[0] : list[index];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Deconstruct<T1, T2>(this KeyValuePair<T1, T2> tuple, out T1 key, out T2 value)
    {
        key = tuple.Key;
        value = tuple.Value;
    }
}