using UnityEngine;
using UnityEditor;

public static class HandleTools
{
    static Matrix4x4 matrix;

    public static void ScopeTo(Transform transform)
    {
        matrix = Handles.matrix;
        Handles.matrix = transform.localToWorldMatrix;
    }

    public static void Unscope()
    {
        Handles.matrix = matrix;
    }
}
