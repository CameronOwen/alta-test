﻿using System.Runtime.CompilerServices;
using UnityEngine;

public static class ColorExtensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color AlphaTo(this Color c, float alpha) => new Color(c.r, c.g, c.b, alpha);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Color AlphaMultiply(this Color c, float multiplier) => new Color(c.r, c.g, c.b, c.a * multiplier);
}
