﻿using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Unoptimized implementation of a priority queue. 
/// </summary>
public class PriorityQueue<T> : IEnumerable<(T item,float priority)>
{
    // TODO: back data structure with Binary or Fibonacci heap
    // instead using the list structure
    // https://en.wikipedia.org/wiki/Binary_heap
    // https://en.wikipedia.org/wiki/Fibonacci_heap
    readonly List<(T item, float priority)> data = new List<(T, float)>(256);
    
    public int Count => data.Count;

    /// <summary>
    /// Add an item to the queue with the given priority
    /// </summary>
    public void Enqueue(T item, float priority)
    {
        data.Add((item, priority));
    }

    /// Remove and return the highest priority item;
    public T Dequeue()
    {
        var priority = 0;
        for (var i = 0; i < data.Count; i++)
        {
            if (data[i].priority < data[priority].priority)
            {
                priority = i;
            }
        }

        var item = data[priority].item;
        data.RemoveAt(priority);
        return item;
    }

    /// <summary>
    /// Clear all items from the queue
    /// </summary>
    public void Clear() => data.Clear();

    IEnumerator<(T item, float priority)> IEnumerable<(T item, float priority)>.GetEnumerator() => data.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)data).GetEnumerator();
}
