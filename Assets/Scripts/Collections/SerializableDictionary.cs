﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializableDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ISerializationCallbackReceiver
{
    [SerializeField] List<TKey> keys;
    [SerializeField] List<TValue> values;

    readonly Dictionary<TKey, TValue> data = new Dictionary<TKey, TValue>();

    #region IDictionary

    public TValue this[TKey key]
    {
        get => data[key];
        set => data[key] = value;
    }

    public int Count => data.Count;

    public ICollection<TKey> Keys => data.Keys;
    public ICollection<TValue> Values => data.Values;
    public void Add(TKey key, TValue value) => data.Add(key, value);
    public bool ContainsKey(TKey key) => data.ContainsKey(key);
    public bool Remove(TKey key) => data.Remove(key);
    public bool TryGetValue(TKey key, out TValue value) => data.TryGetValue(key, out value);
    public void Clear() => data.Clear();

    bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly => (data as ICollection<KeyValuePair<TKey, TValue>>).IsReadOnly;
    void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item) => (data as ICollection<KeyValuePair<TKey, TValue>>).Add(item);
    bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item) => (data as ICollection<KeyValuePair<TKey, TValue>>).Contains(item);
    void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => (data as ICollection<KeyValuePair<TKey, TValue>>).CopyTo(array, arrayIndex);
    bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item) => (data as ICollection<KeyValuePair<TKey, TValue>>).Remove(item);

    IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() => (data as IEnumerable<KeyValuePair<TKey, TValue>>).GetEnumerator();
    public IEnumerator GetEnumerator() => data.GetEnumerator();

    #endregion IDictionary

    #region ISerializationCallbackReceiver

    void ISerializationCallbackReceiver.OnBeforeSerialize()
    {
        keys = new List<TKey>(data.Keys);
        values = new List<TValue>(data.Values);
    }

    void ISerializationCallbackReceiver.OnAfterDeserialize()
    {
        Debug.Assert(keys.Count == values.Count);
        
        Clear();
        
        for (var i = 0; i < keys.Count; ++i)
        {
            Add(keys[i], values[i]);
        }
    }

    #endregion ISerializationCallbackReceiver
}
