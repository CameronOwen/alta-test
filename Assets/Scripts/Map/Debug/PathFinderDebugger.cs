﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
[RequireComponent(typeof(PathFinder))]
public class PathFinderDebugger : MonoBehaviour
{
    [SerializeField] bool editorDebug;
    [SerializeField] Cell debugStart;
    [SerializeField] Cell debugGoal;
    [SerializeField] bool reverseStartGoal;

    [SerializeField] bool stepDebugger;
    [SerializeField] int debugStep = 1;

    [SerializeField] Color startColor = new Color(0.25f, 1f, 0.5f, 0.5f);
    [SerializeField] Color goalColor = new Color(1f, 0.8f, 0.4f, 0.6f);
    [SerializeField] Color activeCellColor= new Color(0f, 1f, 0f, 1f);

    [SerializeField] Color boundryColor = new Color(0.25f, 0.5f, 1f, 1f);
    [SerializeField] Color historyColor = new Color(1f, 0.3f, 1f, 0.2f);
    [SerializeField] Color searchPathColor = new Color(1f, 0.25f, 0.0f, 1f);
    [SerializeField] Color pathFoundColor = new Color(0f, 1f, 0f, 1f);

    Map map;
    Player player;
    PathFinder pathFinder;

    // Only here to force unity to draw enabled checkbox in inspector. 
    void Start() { }

    void OnDrawGizmos()
    {
        if(!enabled) { return; }

        map = map ?? FindObjectOfType<Map>();
        player = player ?? FindObjectOfType<Player>();
        pathFinder = pathFinder ?? GetComponent<PathFinder>();

        if (player == null || map == null || pathFinder == null) { return; }

        var (valid, start, goal) = GetStartAndGoal();

        if (editorDebug && valid && !Application.isPlaying)
        {
            DrawStartAndGoal(debugStart, debugGoal);
            pathFinder.FindPath(map.RuntimeData, start, goal, player.GraphExplorer, stepDebugger ? debugStep : -1);
        } 
        else if (editorDebug && !Application.isPlaying)
        {
            DrawStartAndGoal(debugStart, debugGoal);
            DrawStartAndGoal(start, goal);
            return;
        }

        var (startCell, goalCell, activeCell, boundryCells, history, _, path) = pathFinder.GetDebugData();
        
        var gizmoMatrix = Gizmos.matrix;
        Gizmos.matrix = map.transform.localToWorldMatrix;

        DrawBoundryCells(boundryCells);
        DrawHistoryCells(history);
        DrawStartAndGoal(startCell, goalCell, activeCell);
        DrawPath(activeCell, path, history);

        Gizmos.matrix = gizmoMatrix;
    }

    (bool valid, Cell start, Cell goal) GetStartAndGoal()
    {
        var start = debugStart;
        var goal = debugGoal;

        if (player is PlayerPlatformer playerPlatformer)
        {
            playerPlatformer.UpdateGraph();

            // Ground start/goal...
            var maxSearch = 128;
            for(var i = 0; !map.RuntimeData.IsObstructed(start.Down); i++)
            {
                start = start.Down; 
                if (i > maxSearch) { return (false, debugStart, debugGoal); }
            }
            for (var i = 0; !map.RuntimeData.IsObstructed(goal.Down); i++)
            {
                goal = goal.Down;
                if (i > maxSearch) { return (false, debugStart, debugGoal); }
            }
        } 

        return reverseStartGoal ? (true, goal, start) : (true, start, goal);
    }

    void DrawBoundryCells(PriorityQueue<Cell> boundryCells)
    {
        // Draw boundry cells
        foreach (var item in boundryCells)
        {
            var pos = map.Grid.GetCellCenterLocalFixed(item.Item1);
            Gizmos.color = boundryColor.AlphaMultiply(0.5f);
            Gizmos.DrawCube(pos, map.Grid.cellSize);
            Gizmos.color = boundryColor;
            Gizmos.DrawWireCube(pos, map.Grid.cellSize);
        }
    }

    void DrawHistoryCells(Dictionary<Cell, Cell> history)
    {
        foreach (var (from, to) in history)
        {
            DrawCell(from, historyColor, 0.5f, 0.4f, 0.75f);
            DrawArrow(from, to, historyColor.AlphaMultiply(1.25f));
        }
    }

    void DrawStartAndGoal(Cell startCell, Cell goalCell, Cell? activeCell = null, bool flipColors = false)
    {
        if (flipColors)
        {
            var tempColor = startColor;
            startColor = goalColor;
            goalColor = tempColor;
        }
        DrawCell(startCell, startColor);
        DrawCell(goalCell, goalColor);

        if (activeCell.HasValue)
        {
            DrawCell(activeCell.Value, activeCellColor);
        }

    }

    void DrawCell(Cell cell, Color color, float fill = 0.5f, float border = 1f, float scale = 1f)
    {
        var pos = map.Grid.GetCellCenterLocalFixed(cell);
        Gizmos.color = color.AlphaMultiply(fill);
        Gizmos.DrawCube(pos, map.Grid.cellSize * scale);
        Gizmos.color = color.AlphaMultiply(border);
        Gizmos.DrawWireCube(pos, map.Grid.cellSize * scale);
    }

    void DrawPath(Cell activeCell, List<Cell> path, Dictionary<Cell, Cell> history)
    {
        Gizmos.color = pathFoundColor;
        if (path.Count > 1)
        {
            var p0 = path[0];
            for (var i = 0; i < path.Count; i++)
            {
                var p1 = path[i];
                Gizmos.DrawLine(map.Grid.GetCellCenterLocalFixed(p0), map.Grid.GetCellCenterLocalFixed(p1));
                p0 = p1;
            }
        }
        // Draw path back from active cell
        else
        {
            Gizmos.color = searchPathColor;
            var p0 = activeCell;
            while (history.ContainsKey(p0))
            {
                if (history[p0] == p0) { break; }
                var p1 = history[p0];
                Gizmos.DrawLine(map.Grid.GetCellCenterLocalFixed(p0), map.Grid.GetCellCenterLocalFixed(p1));
                p0 = p1;
            }
        }
    }

    void DrawArrow(Cell from, Cell to, Color color, float gap = 0f)
    {

        var p0 = map.Grid.GetCellCenterLocalFixed(from);
        var p1 = map.Grid.GetCellCenterLocalFixed(to);

        gap *= map.Grid.cellSize.x * 0.5f;

        var direction = (p0 - p1).normalized;
        var ortho = Vector3.Cross(direction, Vector3.forward).normalized;

        var dg = direction * gap;

        Gizmos.color = color;
        Gizmos.DrawLine(p0 - dg, p1 + dg);
        Gizmos.DrawLine(p0 - dg, p0 - dg - direction * 0.15f + ortho * 0.075f);
        Gizmos.DrawLine(p0 - dg, p0 - dg - direction * 0.15f - ortho * 0.075f);
    }
}
