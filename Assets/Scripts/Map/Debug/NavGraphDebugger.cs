﻿using UnityEngine;

[RequireComponent(typeof(PlayerPlatformer))]
public class NavGraphDebugger
    : MonoBehaviour
{
    PlayerPlatformer player;
    [SerializeField] Color walkableCells = new Color(0f, 1f, 1f, 1f);
    [SerializeField] Gradient pathColor;

    // 
    // Only here to force unity to draw enabled checkbox in inspector. 
    void Start() { }


    void OnDrawGizmos()
    {

        if(!enabled) { return; }

        player = player ?? GetComponent<PlayerPlatformer>();

        var (map, navGraph, moves) = player.GetDebugInfo();

        if (navGraph.Nodes.Count == 0 || !Application.isPlaying)
        {
            player.UpdateGraph();
            moves.Clear();
            foreach (var move in GetComponents<PlayerMove>())
            {
                if (move.enabled)
                {
                    moves.Add(move);
                }
            }
        }

        if (!Application.isPlaying)
        {
        }

        var gizmoMatrix = Gizmos.matrix;
        Gizmos.matrix = map.transform.localToWorldMatrix;

        
        foreach (var (cell, node) in navGraph.Nodes)
        {
            var pos = map.Grid.GetCellCenterLocalFixed(cell);
            Gizmos.color = walkableCells.AlphaMultiply(0.5f);
            Gizmos.DrawCube(pos, map.Grid.cellSize);
            Gizmos.color = walkableCells.AlphaMultiply(0.5f);
            Gizmos.DrawWireCube(pos, map.Grid.cellSize);

            var maxCost = 15f;

            foreach (var (linkCell, linkCost) in node.Links)
            {
                var linkPos = map.Grid.GetCellCenterLocalFixed(linkCell);
                var yOffset =  linkPos.y - pos.y > 0 || Mathf.Abs(linkPos.x - pos.x) > 1 ? new Vector3(0f, 0.15f) : new Vector3(0f, -0.15f);
                Gizmos.color = pathColor.Evaluate(linkCost / maxCost);
                Gizmos.DrawLine(pos + yOffset, linkPos + yOffset);
            }
        }

        Gizmos.matrix = gizmoMatrix;
    }
}