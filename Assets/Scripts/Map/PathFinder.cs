using System;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{
    // Search tiles up to x times manhattern distance from goal before aborting.
    // Added because the grid is boundless and goal may be walled in and unreachable.
    [Tooltip("Search tiles up to x times manhattern distance (min 16) from goal before aborting.")]
    [SerializeField] int maxDistanceFactor = 8;

    const int MinDistanceForFactoring = 16;

    // Local caches to reduce memory allocations
    readonly PriorityQueue<Cell> boundryCells = new PriorityQueue<Cell>();
    readonly Dictionary<Cell, Cell> history = new Dictionary<Cell, Cell>(2048);
    readonly Dictionary<Cell, float> distance = new Dictionary<Cell, float>(2048);
    readonly List<Cell> path = new List<Cell>(128);

    Cell start;
    Cell goal;
    Cell activeCell;

    public List<Cell> FindPath(GridData grid, Cell start, Cell goal, Func<Cell, IEnumerable<KeyValuePair<Cell, float>>> linkExplorer, int stepLimit = -1)
    {
        // For debugging
        this.start = start;
        this.goal = goal;

        path.Clear();
        boundryCells.Clear();
        history.Clear();
        distance.Clear();

        // Early out if start/end obstructed
        if (grid.IsObstructed(start) || grid.IsObstructed(goal))
        {
            return new List<Cell>();
        }

        boundryCells.Enqueue(start, 0);
        history[start] = start;
        distance[start] = 0;

#if UNITY_EDITOR
        var step = 0;
#endif
        var searchDistance = Mathf.Max(MinDistanceForFactoring, Cell.ManhattanDistance(start, goal)) * maxDistanceFactor;

        while (boundryCells.Count > 0)
        {
            activeCell = boundryCells.Dequeue();

            if (activeCell == goal) 
            {
                return BuildPath();
            }

            var links = linkExplorer(activeCell);
            foreach (var (linkCell, linkCost) in links)
            {
                // Early outs...
                if (history.ContainsKey(linkCell))    { continue; }
                if (grid.IsObstructed(linkCell)) { continue; }
                if (Cell.ManhattanDistance(start, linkCell) > searchDistance) { continue; }

                var newCost = distance[activeCell] + linkCost;
                
                if (!history.ContainsKey(linkCell) || newCost < distance[linkCell])
                {
                    distance[linkCell] = newCost;
                    boundryCells.Enqueue(linkCell, newCost + Cell.ManhattanDistance(goal, linkCell));
                    history[linkCell] = activeCell;
                }

#if UNITY_EDITOR
                step++;
                if (stepLimit > 0 && step > stepLimit)
                {
                    break;
                }
#endif
            }
#if UNITY_EDITOR
            step++;
            if (stepLimit > 0 && step > stepLimit)
            {
                break;
            }
#endif
        }

        // Goal not found...
        return new List<Cell>();
    }

    List<Cell> BuildPath()
    {
        path.Add(activeCell);

        while (history.ContainsKey(activeCell))
        {
            if (history[activeCell] == activeCell)
            {
                break;
            }
            activeCell = history[activeCell];
            path.Add(activeCell);
        }

        // Return a copy in correct order
        var result = new List<Cell>(path);
        result.Reverse();
        return result;
    }

    // Keeps debugging out of main class. 
    internal (Cell start,
              Cell goal,
              Cell activeCell,
              PriorityQueue<Cell> boundries,
              Dictionary<Cell, Cell> history,
              Dictionary<Cell, float> distance,
              List<Cell> path)
    GetDebugData()
    {
        return (start, goal, activeCell, boundryCells, history, distance, path);
    }

    /*
    // Was allocating a lot of garbage
    IEnumerable<GridCell> Neighbours(GridCell cell)
    {
        yield return cell.Up;
        yield return cell.Right;
        yield return cell.Down;
        yield return cell.Left;
    }
    */

}
