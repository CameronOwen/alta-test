﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A map of grid cells to tiles.
/// </summary>
public interface IGridData : IDictionary<Cell, Tile>
{
    public bool IsObstructed(Cell cell);
}
