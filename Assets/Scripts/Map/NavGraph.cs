﻿using System.Collections.Generic;

public class NavGraph
{
    public Dictionary<Cell, Node> Nodes { get; } = new Dictionary<Cell, Node>(256);

    public void Update(GridData grid, PlayerPlatformer player)
    {
        foreach(var node in Nodes.Values)
        {
            Node.Recycle(node);
        }

        Nodes.Clear();

        // Populate node map;
        foreach(var item in grid)
        {
            var cell = item.Key.Offset(0,1); // want the grid square above defined tiles.
            if (!grid.IsObstructed(cell)) // must have space for player 
            {
                Nodes.Add(cell, Node.Create(cell));
            }
        }

        // Link nodes in map
        foreach(var item in Nodes)
        {
            foreach(var move in player.Moves)
            {
                // Left
                var (cost, cell) = move.Evaluate(item.Key, grid, PlayerMove.Direction.Left);
                if (cost > 0f)
                {
                    item.Value.LinkTo(cell, cost);
                }

                // Right
                (cost, cell) = move.Evaluate(item.Key, grid, PlayerMove.Direction.Right);
                if (cost > 0f)
                {
                    item.Value.LinkTo(cell, cost);
                }
            }
        }
    }
}


