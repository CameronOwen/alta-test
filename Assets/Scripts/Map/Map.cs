using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

[SelectionBase]
[ExecuteAlways]
[RequireComponent(typeof(Grid))]
public class Map : MonoBehaviour
{
    public event Action<Tile> TileChanged;

    // Map of tile prefabs
    [SerializeField] MapData asset;

    Grid grid;

    public MapData Asset => asset;
    
    public GridData RuntimeData { get; } = new GridData();

    public Grid Grid
    {
        get
        {
            if (grid == null) { grid = GetComponent<Grid>(); }
            return grid;
        }
    }

    void Awake()
    {
        if (asset == null) { return; }
        SpawnTiles();
    }

#if UNITY_EDITOR
    void OnValidate()
    {
        // Invoking Instantiate during OnValidate causes SendMessage
        // errors cause Unity?... 
        // This delays the call still after inspectors have updated.
        UnityEditor.EditorApplication.delayCall += SpawnTiles;
    }
#endif

    public Cell WorldToCell(Vector3 position)
    {
        return Grid.WorldToCellFixed(position);
    }

    public Vector3 CellToWorld(Cell cell)
    {
        return Grid.GetCellCenterWorldFixed(cell);
    }


    void SpawnTiles()
    {
#if UNITY_EDITOR
        // This handles a dumb edge case where SpawnTiles() can end up being
        // invoked on a destyoyed instance due to EditorApplication.delayCall
        if (this == null)
        {
            return;
        }
#endif

        ClearRuntimeTiles();

        if (asset == null) { return; }

        var removableTiles = new List<Cell>();
        
        foreach (var (cell, prefab) in asset)
        {
            // Flag null tiles for removal
            if (prefab == null)
            {
                Debug.LogWarning($"Null tile found in MapData {asset.name} at {cell}.\nNull tiles don't need to be saved, just deleted.");
                removableTiles.Add(cell);
                continue;
            }

            SpawnTile(cell, prefab);
        }

        // Remove orphaned cell references
        foreach (var cell in removableTiles)
        {
            SetMapTile(cell, null, false); // keep saved and runtime data in sync.
        }
    }

    void ClearRuntimeTiles()
    {

        foreach (var tile in GetComponentsInChildren<Tile>(true))
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                DestroyImmediate(tile.gameObject);
            }
            else
            {
                Destroy(tile.gameObject);
            }

        }
        RuntimeData.Clear();
    }

    /// <summary>
    /// Instantiates tile from prefab and updates runtile tile map
    /// </summary>
    Tile SpawnTile(Cell cell, Tile prefab)
    {
        var localTilePos = Grid.GetCellCenterLocalFixed(cell);
        if (RuntimeData.TryGetValue(cell, out var tile))
        {
            if (tile != null && tile.gameObject != null)
            {
                DestroyImmediate(tile.gameObject);
            }
        }

        if (prefab == null)
        {
            RuntimeData.Remove(cell);
            return null;
        }

        tile = Instantiate(prefab, transform, false);
        tile.transform.localPosition = localTilePos;
        tile.gameObject.hideFlags = HideFlags.DontSave;
        RuntimeData[cell] = tile;
        return tile;
    }

    /// <summary>
    /// Destroys and respawns all tiles on the map
    /// </summary>
    public void RespawnTiles()
    {
        SpawnTiles();
    }

    /// <summary>
    /// Clears all map tile data
    /// </summary>
    public void ClearTiles()
    {
        ClearRuntimeTiles();
        asset.Clear();
    }

    /// <summary>
    /// Changes the tile prefab associated with the given grid cell
    /// </summary>
    public void SetMapTile(Cell cell, [CanBeNull] Tile prefab, bool notifyChanged = true)
    {
        if (prefab == null)
        {
            asset.Remove(cell);
        }
        else
        {
            asset[cell] = prefab;
        }

        var tile = SpawnTile(cell, prefab);

        if (notifyChanged)
        {
            TileChanged?.Invoke(tile);
        }
    }

}
