﻿using System.Collections.Generic;

public class Node
{

    static Stack<Node> pool = new Stack<Node>(256);

    // Grid cell this node belongs to.
    Cell cell;

    // Links to other grid cells
    readonly Dictionary<Cell, float> links = new Dictionary<Cell, float>(8);

    public Cell Cell => cell;
    public IEnumerable<KeyValuePair<Cell, float>> Links => links;

    public Node(Cell cell)
    {
        this.cell = cell;
    }

    public void LinkTo(Cell cell, float cost)
    {
        // Link does not exist or of lower cost
        if (!links.TryGetValue(cell, out var currentCost) || cost < currentCost)
        {
            links[cell] = cost;
        }
    }

    public static Node Create(Cell cell)
    {
        if (pool.Count > 0)
        {
            var node = pool.Pop();
            node.cell = cell;
            return node;
        }
        return new Node(cell);
    }
    public static void Recycle(Node node)
    {
        node.links.Clear();
        pool.Push(node);
    }

}


