﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

/// <summary>
/// 16 bit grid reference structure that encodes into a single int32.
/// </summary>
[Serializable]
public struct Cell : IEquatable<Cell>
{
    [SerializeField] short _x;
    [SerializeField] short _y;

    public short x
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        get => _x;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        set => _x = value;
    }

    public short y
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        get => _y;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        set => _y = value;
    }

    public Cell Up    => new Cell(x, y + 1);
    public Cell Down  => new Cell(x, y - 1);
    public Cell Left  => new Cell(x - 1, y);
    public Cell Right => new Cell(x + 1, y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)] public Cell(int x, int y) { _x = (short)x; _y = (short)y; }
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public Cell(short x, short y) { _x = x; _y = y; }

    [MethodImpl(MethodImplOptions.AggressiveInlining)] public Cell Offset(int x, int y) => new Cell(_x + x, _y + y);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public Cell Offset(short x, short y) => new Cell(_x + x, _y + y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)] public int ManhattanDistance(Cell to) => ManhattanDistance(this, to);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public float Distance(Cell to) => Distance(this, to);

    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static int ManhattanDistance(Cell a, Cell b) => Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static float Distance(Cell a, Cell b) { var x = a.x - b.x; var y = a.y - b.y; return (float)Math.Sqrt(x * x + y * y); }
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static int Encode(Cell c) => ((int)c.x | (int)c.y << 16);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static Cell Decode(int n) => new Cell((short)(n & 0xFFFF), (short)(n >> 16));

    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static Cell operator +(Cell a, Cell b) => new Cell(a.x + b.x, a.y + b.y);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static Cell operator -(Cell a, Cell b) => new Cell(a.x - b.x, a.y - b.y);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static Cell operator -(Cell c) => new Cell(-c.x, -c.y);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static Cell operator *(Cell a, Cell b) => new Cell(a.x * b.x, a.y * b.y);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static Cell operator *(Cell c, int n) => new Cell(c.x * n, c.y * n);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static Cell operator /(Cell a, Cell b) => new Cell(a.x / b.x, a.y / b.y);
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static Cell operator /(Cell c, int n) => new Cell(c.x / n, c.y / n);

    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static bool operator ==(Cell lhs, Cell rhs) => lhs.x == rhs.x && lhs.y == rhs.y;
    [MethodImpl(MethodImplOptions.AggressiveInlining)] public static bool operator !=(Cell lhs, Cell rhs) => !(lhs == rhs);

    public static implicit operator int(Cell c) => Encode(c);
    public static implicit operator Cell(int n) => Decode(n);

    public static implicit operator Vector2(Cell c) => new Vector2(c.x, c.y);
    public static implicit operator Vector2Int(Cell c) => new Vector2Int(c.x, c.y);
    public static implicit operator Vector3Int(Cell c) => new Vector3Int(c.x, c.y, 0);
    public static implicit operator Cell(Vector2Int v) => new Cell(v.x, v.y);
    public static implicit operator Cell(Vector3Int v) => new Cell(v.x, v.y);

    public override string ToString() => ToString(null, CultureInfo.InvariantCulture.NumberFormat);

    public string ToString(string format) => ToString(format, CultureInfo.InvariantCulture.NumberFormat);

    public string ToString(string format, IFormatProvider formatProvider) => string.Format(CultureInfo.InvariantCulture.NumberFormat, "({0}, {1})", x.ToString(format, formatProvider), y.ToString(format, formatProvider));

    public override bool Equals(object obj) => obj is Cell cell && Equals(cell);

    public bool Equals(Cell other) => x == other.x && y == other.y;

    public override int GetHashCode()
    {
        return Encode(this);
    }

}
