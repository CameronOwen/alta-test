﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GridData : IGridData
{
    [SerializeField] SerializableDictionary<Cell, Tile> data = new SerializableDictionary<Cell, Tile>();

    public bool IsObstructed(Cell cell)
    {
        return data.ContainsKey(cell);
    }

    // IGridData implimentaiton via SerializableDictionary data
    #region IGridData

    public Tile this[Cell key] { get => data[key]; set => data[key] = value; }
    public ICollection<Cell> Keys => data.Keys;
    public ICollection<Tile> Values => data.Values;
    public int Count => data.Count;
    public bool IsReadOnly => ((ICollection<KeyValuePair<Cell, Tile>>)data).IsReadOnly;

    public void Add(Cell key, Tile value) => data.Add(key, value);
    public bool ContainsKey(Cell key) => data.ContainsKey(key);
    public bool Remove(Cell key) => data.Remove(key);
    public bool TryGetValue(Cell key, out Tile value) => data.TryGetValue(key, out value);

    public void Add(KeyValuePair<Cell, Tile> item) => ((ICollection<KeyValuePair<Cell, Tile>>)data).Add(item);
    public void Clear() => ((ICollection<KeyValuePair<Cell, Tile>>)data).Clear();
    public bool Contains(KeyValuePair<Cell, Tile> item) => ((ICollection<KeyValuePair<Cell, Tile>>)data).Contains(item);
    public void CopyTo(KeyValuePair<Cell, Tile>[] array, int arrayIndex) => ((ICollection<KeyValuePair<Cell, Tile>>)data).CopyTo(array, arrayIndex);
    public bool Remove(KeyValuePair<Cell, Tile> item) => ((ICollection<KeyValuePair<Cell, Tile>>)data).Remove(item);

    public IEnumerator<KeyValuePair<Cell, Tile>> GetEnumerator() => ((IEnumerable<KeyValuePair<Cell, Tile>>)data).GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => data.GetEnumerator();

    #endregion IGridData

}
