using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(Map))]
public class MapEditor : Editor
{
    Map map;
    SerializedObject mapDataSO;
    Color editorColor = new Color(0f, 1f, 1f);
    bool showMapData = false;
    bool showCellCoords = false;

    Cell cellWorldCoord;

    void OnEnable()
    {
        map = (Map)target;
        Undo.undoRedoPerformed -= OnUndoRedo;
        Undo.undoRedoPerformed += OnUndoRedo;
    }

    void OnDisable()
    {
        
        Undo.undoRedoPerformed -= OnUndoRedo;
    }

    void OnUndoRedo() 
    {
        if(map != null && EditorUtility.IsDirty(map.Asset))
        {
            map.RespawnTiles();
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        DrawMapDataInspector();

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Respawn Tiles"))
        {
            map.RespawnTiles();
        }

        if (GUILayout.Button("Clear Map"))
        {
            if (EditorUtility.DisplayDialog("Clear map", "This will delete all saved map data.", "OK", "Cancel"))
            {
                map.ClearTiles();
            }
        }

        EditorGUILayout.EndHorizontal();

        showCellCoords = EditorGUILayout.Toggle("Show cell coordinates", showCellCoords);


    }

    void DrawMapDataInspector()
    {
        if (map.Asset == null) { return; }

        EditorGUILayout.LabelField("Tile Count", map.Asset.Count.ToString());

        if (mapDataSO == null || mapDataSO?.targetObject != map.Asset)
        {
            mapDataSO = new SerializedObject(map.Asset);
        }

        showMapData = EditorGUILayout.Foldout(showMapData, map.Asset.name);
        if (showMapData)
        {
            mapDataSO.Update();
            EditorGUI.indentLevel++;
            DrawPropertiesExcluding(mapDataSO, "m_Script", "data");
            EditorGUI.indentLevel--;
            mapDataSO.ApplyModifiedProperties();
        }
    }

    void OnSceneGUI()
    {
        // Prevent Map deselection when clicking in scene view.
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        if (map.Asset == null) { return; }

        switch (Event.current.type)
        {
            case EventType.MouseMove: OnMouseMove(); break;
            case EventType.MouseDown: OnMouseDown(); break;
            case EventType.Repaint:   OnRepaint();   break;
        }
    }

    void OnMouseMove() 
    {
        var mouseScreenPos = Event.current.mousePosition;
        var mouseWorldRay = HandleUtility.GUIPointToWorldRay(mouseScreenPos);
        var mouseWorldPos = mouseWorldRay.origin;

        // Assumes 2D scene view and grid is aligned with scene view z plane. 
        mouseWorldPos.z = map.transform.position.z;
        cellWorldCoord = map.WorldToCell(mouseWorldPos);
    }

    void OnMouseDown() 
    {

        if(map.Asset.Prefabs.Count == 0)
        {
            Debug.LogWarning("Unable to change map cell, no prefabs defined in map data.");
        }

        // Right Click...
        if (Event.current.button == 1)
        {
            Undo.RegisterCompleteObjectUndo(map.Asset, $"Clear {map.Asset.name} tile.");
            map.SetMapTile(cellWorldCoord, null);
            return;
        }

        // Middle or some other kind of click...
        if (Event.current.button > 1)
        {
            return;
        }

        // Left click...
        //Event.current.Use();

        var prefabIndex = 0;
        if (map.Asset.TryGetValue(cellWorldCoord, out var tilePrefab))
        {
            prefabIndex = map.Asset.Prefabs.IndexOf(tilePrefab);
            prefabIndex = (prefabIndex < 0) ? 0 : prefabIndex + 1;
        }

        // Clear tile
        if (prefabIndex >= map.Asset.Prefabs.Count)
        {
            Undo.RegisterCompleteObjectUndo(map.Asset, $"Clear {map.Asset.name} tile.");
            map.SetMapTile(cellWorldCoord, null);
        }

        // Set Tile
        else
        {
            Undo.RegisterCompleteObjectUndo(map.Asset, $"Change {map.Asset.name} tile.");
            map.SetMapTile(cellWorldCoord, map.Asset.Prefabs[prefabIndex]);
        }
        
        EditorUtility.SetDirty(map.Asset);
    }

    void OnRepaint()
    {
        using (new Handles.DrawingScope(GUI.skin.settings.selectionColor, map.transform.localToWorldMatrix))
        {
            var xSize = map.Grid.cellSize.x * 0.5f;
            var ySize = map.Grid.cellSize.y * 0.5f;
            var pos = map.Grid.GetCellCenterLocalFixed(cellWorldCoord);
            //Handles.DrawWireCube(pos, map.Grid.cellSize);
            Handles.DrawSolidRectangleWithOutline(new Vector3[4] 
            {
                new Vector3(pos.x-xSize, pos.y-ySize, pos.z),
                new Vector3(pos.x+xSize, pos.y-ySize, pos.z),
                new Vector3(pos.x+xSize, pos.y+ySize, pos.z),
                new Vector3(pos.x-xSize, pos.y+ySize, pos.z)
            }, GUI.skin.settings.selectionColor, GUI.skin.settings.selectionColor * 2);

            if (showCellCoords)
            {
                var offset = (Vector3)(new Vector2(-1f, 0f) * map.Grid.cellSize * 0.25f);
                GUI.backgroundColor = Color.black.AlphaMultiply(0.25f);
                GUI.color = Color.white.AlphaMultiply(2);
                
                //GUI.contentColor = Color.black.AlphaMultiply(0.5f);
                foreach (var cell in map.Asset.Keys)
                {
                    pos = map.Grid.GetCellCenterLocalFixed(cell);
                    Handles.Label(pos + offset * HandleUtility.GetHandleSize(pos), $"{cell.x},{cell.y}", (GUIStyle)"ShurikenObjectField");
                }
            }
        }

        SceneView.currentDrawingSceneView.Repaint();
    }



}