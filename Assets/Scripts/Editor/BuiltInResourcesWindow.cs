using System;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class BuiltInResourcesWindow : EditorWindow
{
    [MenuItem("Window/Built-in styles and icons")]
    public static void ShowWindow()
    {
        var w = GetWindow<BuiltInResourcesWindow>();
        w.Show();
    }

    struct Drawing
    {
        public Rect Rect;
        public Action Draw;
    }

    List<Drawing> Drawings;

    List<UnityEngine.Object> objects;
    float scrollPos;
    float maxY;
    Rect oldPos;

    bool showingStyles = true;
    bool showingIcons  = false;

    string search = "";

    void OnGUI()
    {
        if (position.width != oldPos.width && Event.current.type == EventType.Layout)
        {
            Drawings = null;
            oldPos = position;
        }

        GUILayout.BeginHorizontal();

        if (GUILayout.Toggle(showingStyles, "Styles", EditorStyles.toolbarButton) != showingStyles)
        {
            showingStyles = !showingStyles;
            showingIcons = !showingStyles;
            Drawings = null;
        }

        if (GUILayout.Toggle(showingIcons, "Icons", EditorStyles.toolbarButton) != showingIcons)
        {
            showingIcons = !showingIcons;
            showingStyles = !showingIcons;
            Drawings = null;
        }

        GUILayout.EndHorizontal();

        var newSearch = GUILayout.TextField(search);
        if (newSearch != search)
        {
            search = newSearch;
            Drawings = null;
        }

        float top = 36;

        if (Drawings == null)
        {
            var lowerSearch = search.ToLower();

            Drawings = new List<Drawing>();

            var inactiveText = new GUIContent("inactive");
            var activeText = new GUIContent("active");

            var x = 5.0f;
            var y = 5.0f;

            if (showingStyles)
            {
                foreach (var ss in GUI.skin.customStyles)
                {
                    if (lowerSearch != "" && !ss.name.ToLower().Contains(lowerSearch))
                    {
                        continue;
                    }

                    var thisStyle = ss;
                    var draw      = new Drawing();
                    var buttonSize = GUI.skin.button.CalcSize(new GUIContent(ss.name));
                    var width =  ss.CalcSize(inactiveText).x + ss.CalcSize(activeText).x;
                    width = Mathf.Max(100.0f, buttonSize.x, width) + 16.0f;

                    var height = 60.0f;

                    if (x + width > position.width - 32 && x > 5.0f)
                    {
                        x = 5.0f;
                        y += height + 10.0f;
                    }

                    draw.Rect = new Rect(x, y, width, height);

                    width -= 8.0f;

                    draw.Draw = () =>
                    {
                        if (GUILayout.Button(thisStyle.name, GUILayout.Width(width)))
                        {
                            CopyText("(GUIStyle)\"" + thisStyle.name + "\"");
                        }

                        GUILayout.BeginHorizontal();
                        GUILayout.Toggle(false, inactiveText, thisStyle, GUILayout.Width(width / 2));
                        GUILayout.Toggle(false, activeText, thisStyle, GUILayout.Width(width / 2));
                        GUILayout.EndHorizontal();
                    };

                    x += width + 18.0f;

                    Drawings.Add(draw);
                }
            }
            else if (showingIcons)
            {
                if (objects == null)
                {
                    objects = new List<UnityEngine.Object>(Resources.FindObjectsOfTypeAll(typeof(Texture2D)));
                    objects.Sort((pA, pB) => System.String.Compare(pA.name, pB.name, StringComparison.OrdinalIgnoreCase));
                }

                var rowHeight = 0.0f;

                foreach (var oo in objects)
                {
                    var texture = (Texture2D)oo;

                    if (texture.name == "") { continue; }
                    if (lowerSearch != "" && !texture.name.ToLower().Contains(lowerSearch)) { continue; }

                    var draw = new Drawing();
                    var width = Mathf.Max(GUI.skin.button.CalcSize(new GUIContent(texture.name)).x,                        texture.width                    ) + 8.0f;
                    var height = texture.height + GUI.skin.button.CalcSize(new GUIContent(texture.name)).y + 8.0f;

                    if (x + width > position.width - 32.0f)
                    {
                        x = 5.0f;
                        y += rowHeight + 8.0f;
                        rowHeight = 0.0f;
                    }

                    draw.Rect = new Rect(x, y, width, height);
                    rowHeight = Mathf.Max(rowHeight, height);
                    width -= 8.0f;

                    draw.Draw = () =>
                    {
                        if (GUILayout.Button(texture.name, GUILayout.Width(width)))
                        {
                            CopyText("EditorGUIUtility.FindTexture( \"" + texture.name + "\" )");
                        }

                        var textureRect = GUILayoutUtility.GetRect(texture.width, texture.width, texture.height, texture.height, GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false));
                        EditorGUI.DrawTextureTransparent(textureRect, texture);
                    };

                    x += width + 8.0f;

                    Drawings.Add(draw);
                }
            }

            maxY = y;
        }

        var r = position;
        r.y = top;
        r.height -= r.y;
        r.x = r.width - 16;
        r.width = 16;

        var areaHeight = position.height - top;
        scrollPos = GUI.VerticalScrollbar(r, scrollPos, areaHeight, 0.0f, maxY);

        var area = new Rect(0, top, position.width - 16.0f, areaHeight);
        GUILayout.BeginArea(area);

        var count = 0;
        foreach (var draw in Drawings)
        {
            var newRect = draw.Rect;
            newRect.y -= scrollPos;

            if (newRect.y + newRect.height > 0 && newRect.y < areaHeight)
            {
                GUILayout.BeginArea(newRect, GUI.skin.textField);
                draw.Draw();
                GUILayout.EndArea();

                count++;
            }
        }

        GUILayout.EndArea();
    }

    void CopyText(string pText)
    {
        var editor = new TextEditor();
        editor.text = pText;
        editor.SelectAll();
        editor.Copy();
    }
}